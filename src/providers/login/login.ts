import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {AngularFireDatabase} from "angularfire2/database";
import 'rxjs/add/operator/map';

import {Storage} from "@ionic/storage";

@Injectable()
export class LoginProvider {

  private PATH = 'login/';

  constructor(public http: HttpClient, private db: AngularFireDatabase, private storage: Storage) {
  }

  getLoginAll()
  {
    return this.db.list(this.PATH)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

  saveLogin(contact:any)
  {
    return new Promise((resolve, reject) => {
      if (contact.key) {
        this.db.list(this.PATH)
          .update(contact.key, {
            user: contact.user,
            password: contact.password
          })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH)
          .push({
            user: contact.user,
            password: contact.password
          })
          .then(() => resolve());
      }
    })
  }

  getSession()
  {
    return this.storage.get('access');
  }


}

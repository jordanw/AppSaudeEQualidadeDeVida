import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import 'rxjs/add/operator/map';
import firebase from 'firebase';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {Storage} from '@ionic/storage';

@Injectable()
export class ServersProvider {

  private PATH = 'server/';


  constructor(
    public http: HttpClient,
    private db: AngularFireDatabase,
    private localNotifications: LocalNotifications,
    private storage: Storage)
  {

  }

  getAll()
  {
    return this.db.list(this.PATH, ref => ref.orderByChild('evaluate_date'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }


  get(key:string)
  {
    return this.db.object(this.PATH + key).snapshotChanges()
      .map(c=>{
        return {key:c.key, ...c.payload.val()};
      })
  }


  save(contact:any)
  {
    return new Promise((resolve, reject) => {
      if (contact.key) {
        this.db.list(this.PATH)
          .update(contact.key, {
            name: contact.name,
            office: contact.office,
            departament: contact.departament,
            process: contact.process,
            reason: contact.reason,
            evaluate_date: String(this.recentDate(contact._evaluate.quarterly.next_date, contact._evaluate.bi_annually.next_date)),
            _delete: contact._delete,
            type_number: contact.type_number,
            type_process: contact.type_process,
            contact: contact.contact,
            _evaluate:{
              bi_annually:{
                next_date: contact._evaluate.bi_annually.next_date,
                last_date: contact._evaluate.bi_annually.last_date
              },
              quarterly:{
                next_date: contact._evaluate.quarterly.next_date,
                last_date: contact._evaluate.quarterly.last_date
              }
            }
              })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH)
          .push({
            name: contact.name,
            office: contact.office,
            departament: contact.departament,
            process: contact.process,
            reason: contact.reason,
            evaluate_date:this.recentDate(contact._evaluate.quarterly.next_date, contact._evaluate.bi_annually.next_date),
            _delete: contact._delete,
            type_number: contact.type_number,
            type_process: contact.type_process,
            contact: contact.contact,
            _evaluate:{
              bi_annually:{
                next_date: contact._evaluate.bi_annually.next_date,
                last_date: contact._evaluate.bi_annually.last_date
              },
              quarterly:{
                next_date: contact._evaluate.quarterly.next_date,
                last_date: contact._evaluate.quarterly.last_date
              }
            }
          })
          .then(() => resolve());
      }
    })
  }


  remove(key:string)
  {
    return this.db.list(this.PATH).remove(key);
  }


  recentDate(date:string, date2:string )
  {

    let day = date.slice(8,10);
    let month = date.slice(5,7);
    let year = date.slice(0,4);

    let day2 = date2.slice(8,10);
    let month2 = date2.slice(5,7);
    let year2 = date2.slice(0,4);

    let dayOne = Number(day) + Number(month) + Number(year);
    let dayTwo = Number(day2) + Number(month2) + Number(year2);

    let dateString:string = '';

    if(dayOne <= dayTwo)
    {
      dateString += year + "/";
      dateString += month + "/";
      dateString += day;
      return dateString;
    }
    else {
      dateString += year2 + "/";
      dateString += month2 + "/";
      dateString += day2;
      return dateString;
    }

  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {AngularFireDatabase} from "angularfire2/database";
import 'rxjs/add/operator/map';


@Injectable()
export class RemarkProvider {

  private PATH = 'remark/';

  constructor(public http: HttpClient, private db: AngularFireDatabase) {
    //console.log('Hello RemarkProvider Provider');
  }

  getAll()
  {
    return this.db.list(this.PATH, ref => ref.orderByChild('evaluate_date'))
      .snapshotChanges()
      .map(changes => {
        //console.log('ARGUMENTO CHANGES');
        //console.log(changes);
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }


  get(key:string)
  {
    return this.db.object(this.PATH + key).snapshotChanges()
      .map(c=>{
        return {key:c.key, ...c.payload.val()};
      })
  }

  save(contact:any)
  {
    return new Promise((resolve, reject) => {
      if (contact.key) {
        this.db.list(this.PATH)
          .update(contact.key, {
            _key: contact._key,
            title: contact.title,
            _date: contact._date,
            note: contact.note,
          })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH)
          .push({
            _key: contact._key,
            title: contact.title,
            _date: contact._date,
            note: contact.note,
          })
          .then(() => resolve());
      }
    })
  }


  remove(key:string)
  {
    return this.db.list(this.PATH).remove(key);
  }


}

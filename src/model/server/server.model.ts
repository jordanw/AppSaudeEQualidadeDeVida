export interface ServerModel {
  key?: string;
  name: string;
  office: string;
  departament: string;
  process: string;
  reason:string;
  evaluate_date:string;
  /**********
   * FIELD ADDED
   * ADD contacts
   * tres dias antes - trimestral
   * bianual - 15 dias antes
   * add - adiar
   **/
  //status_initial:string;
  _delete:number; /* Aposentar */
  type_number:number; /* UFMA, HU */
  type_process:string;
  contact:string;
  /*****************
   *
   */
  /**
   * change names */
  _evaluate: {
    bi_annually:{
      last_date: string;
      next_date: string;
    };
    quarterly: {
      last_date: string;
      next_date: string;
    };
  };
}

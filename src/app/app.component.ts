import { Component } from '@angular/core';
import { Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {IndexPage} from "../pages/index";
import {LoginProvider} from "../providers/login/login";
import { ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import {ServersProvider} from "../providers/servers/servers";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    session:LoginProvider,
    private toast: ToastController,
    private network: Network,
    private providerServer: ServersProvider
  )
  {
    platform.ready().then(() => {
      console.log(session.getSession().then(value => {
        if(value == null)
        {
          this.rootPage = LoginPage;
        }else{
          this.rootPage = IndexPage;
        }
      }));

      this.ionViewDidEnter();
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ionViewDidEnter()
  {
    this.network.onDisconnect().subscribe(data => {
      console.log(data);
      this.toast.create({
        message: "Seu dispositivo está desconectado",
        duration: 3000
      }).present();

    }, error => console.error(error));
  }

}

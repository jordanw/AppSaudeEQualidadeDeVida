import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import {IndexPageModule} from "../pages/index/index.module";
import {PerfilPageModule} from "../pages/perfil/perfil.module";
import {LoginPageModule} from "../pages/login/login.module";
import {RemarkPageModule} from "../pages/remark/remark.module";
import {EditRemarkPageModule} from "../pages/edit-remark/edit-remark.module";


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


/**************** INSTANCE OF FIREBASE **************/
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CONFIG } from './firebase.credentials';


/* **************************************************/

import {HttpClientModule} from "@angular/common/http";
import { ServersProvider } from '../providers/servers/servers';
import { LoginProvider } from '../providers/login/login';
import { RemarkProvider } from '../providers/remark/remark';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import {LocalNotifications} from '@ionic-native/local-notifications';

@NgModule({
  declarations: [
    MyApp,


  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IndexPageModule,
    PerfilPageModule,
    LoginPageModule,
    RemarkPageModule,
    EditRemarkPageModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    HttpClientModule,
   IonicStorageModule.forRoot()


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServersProvider,
    LoginProvider,
    Network,
    RemarkProvider,
    LocalNotifications

  ]
})
export class AppModule {}

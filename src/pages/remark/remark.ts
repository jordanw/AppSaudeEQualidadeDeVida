import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Observable} from "rxjs/Observable";
import {RemarkProvider} from "../../providers/remark/remark";
import {EditRemarkPage} from "../edit-remark/edit-remark";
import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-remark',
  templateUrl: 'remark.html',
})
export class RemarkPage {

  server: any;
  servers: Observable<any>;
  notes: Array<string> = [];
  datas:Array<string> = [];
  titles:Array<string> = [];
  key:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private provider: RemarkProvider
  )
  {

    this.server = this.navParams.get('server');
    this.key = this.navParams.get('key');
    this.servers = this.provider.getAll();
    this.ionViewWillEnter();

  }

  ionViewDidEnter()
  {

    this.addRemark();
  }

  ionViewWillEnter()
  {
    this.addRemark();
  }

addRemark(){
    let databse = firebase.database();
    let ref = databse.ref('remark/').on('value',(data) =>{
    var scores = data.val();
    var keys= Object.keys(scores);

    for(var i = 0; i < keys.length; i++)
    {
      let k = keys[i];
      if(scores[k]._key == this.key)//this.server.key
      {
        //console.log('_KEY/KEY: ', scores[k]._key, this.key);
        if(!(this.notes.indexOf(scores[k].note) > -1))
        {
          //console.log('entrou nas notas');
          this.notes.push(scores[k].note);
          this.datas.push(scores[k]._date);
          this.titles.push(scores[k].title);

        }

      }
    }

    //console.log('array de notes: ', this.notes);
    //console.log('array de dates: ', this.datas);

    });
};

  insertRemark(server)
  {
    this.navCtrl.push(EditRemarkPage, {
      'server':server,
      'key':this.key
    });
  }

  doRefresh(refresher)
  {
    this.addRemark();
    refresher.complete();
  }


}

import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import {Validators, FormBuilder} from "@angular/forms";
import {IndexPage} from "../index";
import firebase from 'firebase';
import {ServersProvider} from "../../providers/servers/servers";
import {LoginProvider} from "../../providers/login/login";
import {RemarkProvider} from "../../providers/remark/remark";
import {Storage} from '@ionic/storage';
import {LoginModel} from "../../model/login/login.model";
import {ServerModel} from "../../model/server/server.model";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  access:any = {};
  user:string;
  password:string;

  constructor(
    public navCtrl: NavController,
    private provider: ServersProvider,
    private loginProvider: LoginProvider,
    private remarkProvider: RemarkProvider,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private storage: Storage )
  {

    this.access = this.formBuilder.group({
      user:['', Validators.required],
      password:['', Validators.required],
    });

  }

  pageIndex()
  {
    let {user,password} = this.access.controls;
    let databse = firebase.database();
    let ref = databse.ref('login/').on('value',(data) =>{
    var scores = data.val();
    var keys= Object.keys(scores);

    for(var i = 0; i < keys.length; i++)
    {

      var k = keys[i];
      if(user.value == scores[k].user && password.value == scores[k].password)
      {
        this.storage.set('access', 'yes');
        return this.navCtrl.setRoot(IndexPage);
      }
      else{
        this.toast.create({message:'Usuário ou senha inválidos', duration: 3000}).present();
        return false;
      }
    }

  });
}

}

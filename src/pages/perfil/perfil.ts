import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import {LoginPage} from "../login/login";
import {RemarkPage} from "../remark/remark";
import {Storage} from "@ionic/storage";
import {ServerModel} from "../../model/server/server.model";
import {ServersProvider} from "../../providers/servers/servers";


@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  server:any;
  last_date:any;
  next_date:any;
  type:string;
  typeAvaliation:string;
  key:any;

  constructor(public navCtrl: NavController, private toast: ToastController, public navParams: NavParams, private storage: Storage, private provider: ServersProvider)
  {
    this.server = this.navParams.get('server');
    this.key = this.navParams.get('key');
    this.last_date = this.defineDate(this.server._evaluate.bi_annually.last_date.slice(0,10));
    this.next_date = this.defineDate(this.server._evaluate.bi_annually.next_date.slice(0,10));

    if(this.server.type_number == 0)
    {
      this.type = "HU";
    }
    else
      {
      this.type = "UFMA";
    }

  }


  defineDate(server:any)
  {
    let year = server.slice(0,4);
    let month = server.slice(5,7);
    let day = server.slice(8,10);
    return new Date(year,month-1, day).toISOString();
  }

  defineAvaliation()
  {
    if(this.typeAvaliation == "p")
    {
      this.last_date = this.defineDate(this.server._evaluate.bi_annually.last_date);
      this.next_date = this.defineDate(this.server._evaluate.bi_annually.next_date);
    }else if(this.typeAvaliation == "r")
    {
      this.last_date = this.defineDate(this.server._evaluate.quarterly.last_date);
      this.next_date = this.defineDate(this.server._evaluate.quarterly.next_date);

    }

  }

  getRemark(server)
  {
    this.navCtrl.push(RemarkPage, {
      'server': server,
      'key': this.key
    });
  }

  public logout()
  {
    this.storage.clear();
    this.navCtrl.setRoot(LoginPage);
  }

  savePerfil()
  {
    let serverUpdate = {};
    serverUpdate['key'] = this.key;
    serverUpdate['name'] = this.server.name;
    serverUpdate['office'] = this.server.office;
    serverUpdate['departament'] = this.server.departament;
    serverUpdate['process'] = this.server.process;
    serverUpdate['reason'] = this.server.reason;
    serverUpdate['_delete'] =  this.server._delete;
    serverUpdate['evaluate_date'] = this.server.evaluate_date;
    serverUpdate['type_number'] = this.server.type_number;
    serverUpdate['type_process'] = this.server.type_process;
    serverUpdate['contact'] = this.server.contact;
    serverUpdate['_evaluate'] = {
      bi_annually:{
        next_date:this.server._evaluate.quarterly.next_date,
        last_date: this.server._evaluate.quarterly.next_date
      },
        quarterly:{
          next_date: this.server._evaluate.quarterly.next_date,
          last_date:this.server._evaluate.quarterly.next_date
        }
  };

    if(this.typeAvaliation == "p" || this.typeAvaliation == undefined)
    {
      serverUpdate['_evaluate'] = {
        bi_annually:{
          next_date: this.next_date.slice(0,10),
          last_date: this.last_date.slice(0,10)
        },
        quarterly:{
          next_date: this.server._evaluate.quarterly.next_date,
          last_date: this.server._evaluate.quarterly.last_date
        }
    };
      this.server._evaluate.bi_annually.next_date = this.next_date.slice(0,10);
      this.server._evaluate.bi_annually.last_date = this.last_date.slice(0,10);
    }

    if(this.typeAvaliation == "r") {

      serverUpdate['_evaluate'] = {
        bi_annually: {
          next_date: this.server._evaluate.bi_annually.next_date,
          last_date: this.server._evaluate.bi_annually.last_date
        },
        quarterly: {
          next_date: this.next_date.slice(0, 10),
          last_date: this.last_date.slice(0, 10)
        }
      };
      this.server._evaluate.quarterly.next_date = this.next_date.slice(0, 10);
      this.server._evaluate.quarterly.last_date = this.last_date.slice(0, 10);
    }

    if(this.provider.save(serverUpdate)){
      this.toast.create({message:'Avaliações alteradas com sucesso', duration: 3000}).present();
    }
  }

  }



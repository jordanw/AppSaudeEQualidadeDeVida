import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {PerfilPage} from "../perfil/perfil";
import {LoginPage} from "../login/login";
import {ServersProvider} from "../../providers/servers/servers";
import firebase from 'firebase';
import {AngularFireDatabase} from "angularfire2/database";
import {Storage} from "@ionic/storage";
import { Platform} from 'ionic-angular';
import {LocalNotifications} from "@ionic-native/local-notifications";


@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})

export class IndexPage {

  public countryList:Array<any>;
  public loadedCountryList:Array<any>;
  public countryKeys:Array<any> = [];
  public countryRef:firebase.database.Reference;
  public dateAvaliation :Array<any> = [];
  public serverToday: Array<any> = [];
  public serverKeyToday: Array<any> = [];
  private todayStatus: boolean = false;
  public evaluateDate:any;


  constructor(
    public navCtrl: NavController,
    private provider: ServersProvider,
    private db: AngularFireDatabase,
    private storage: Storage,
    private localNotifications: LocalNotifications,
    platform: Platform,

    )
  {
    platform.ready().then(() => {
      this.countryRef = firebase.database().ref('server/');
      let dataAtual = new Date();//.toISOString().slice(0,10);
      var teste2 = this.countryRef.orderByChild('evaluate_date').equalTo(dataAtual.toISOString().slice(0,10).split('-').join('/'))
        .on('value', (v)=>{
          this.todayStatus = true;
          let teste = [];
          let keys = [];
          v.forEach( value => {
            keys.push(value.key);
            teste.push(value.val());
            this.localNotifications.schedule({
              title: 'Avaliação',
              text: `Servidores possuem avaliação em ${value.val().evaluate_date.split('/').reverse().join('/')}`,
              vibrate: true,
              launch: true
            });
          });
          this.serverToday = teste;
          this.serverKeyToday = keys;
          //console.log('server todat', this.serverToday);
          //console.log('server todat key', this.serverKeyToday);
        });


      /*this.provider.getAlertNotify().then((value)=>{
        if( value != 'alerted')
        {
          this.provider.alertNotification(0);
        }

      });*/
      this.startServer();
      //this.ionViewWillEnter();
    });

  }

  ionViewWillEnter()
  {
    this.startServer();
  }


  startServer()
  {
    this.countryRef.on('value', countryList => {
      let countries = [];
      let keys = [];
      let dates = [];

      countryList.forEach( country => {
        countries.push(country.val());
        dates.push(new Date(country.val().evaluate_date).toLocaleDateString());
        //console.log('aray de datas formatadas', dates);
        //console.log('Objeto data de evaluate date: ', new Date(country.val().evaluate_date).toLocaleDateString());
        keys.push(country.key);
        return false;
      });

      this.countryList = countries;
      this.loadedCountryList = countries;
      this.countryKeys = keys;
      this.dateAvaliation = dates;
    });

  }

  initializeItems(): void {
    this.countryList = this.loadedCountryList;
  }

  getItems(searchbar) {
    this.initializeItems();
    var q = searchbar.srcElement.value;
    if (!q) {
      return;
    }

    this.countryList = this.countryList.filter((v) => {
      if(v.name && q) {
        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });

    //console.log(q, this.countryList.length);

  }


  editContact(server: any, key:any)
  {
    this.navCtrl.push('PerfilPage', {'server': server, 'key': key});
  }

  public logout()
  {
    this.storage.clear();
    this.navCtrl.setRoot(LoginPage);
  }



  doRefresh(refresher) {
    this.countryRef = firebase.database().ref('server/');
    refresher.complete();
  }


}

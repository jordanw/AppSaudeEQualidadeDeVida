import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Validators, FormBuilder} from "@angular/forms";

import {RemarkProvider} from "../../providers/remark/remark";

@IonicPage()
@Component({
  selector: 'page-edit-remark',
  templateUrl: 'edit-remark.html',
})
export class EditRemarkPage {

  server:any;
  register:any = {};
  remark:any = {};
  teste:string;
  title:any;
  titleNote:any;
  key:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private provider: RemarkProvider,
    private toast: ToastController)
  {
    this.server = this.navParams.get('server');
    this.key = this.navParams.get('key');

    this.register = this.formBuilder.group({
      remarkText:['', Validators.required],
      title:['', Validators.required],
    });
  }

  registerRemark(server:any)
  {
      var date = new Date();
    if(this.teste != null && this.teste != '' && this.teste != undefined &&
      this.titleNote != null && this.titleNote != '' && this.titleNote != undefined
    )
    {
      this.remark._key = this.key;
      this.remark.note= this.teste;
      this.remark.title = this.titleNote;
      this.remark._date = date.toISOString().slice(0,10);
      this.provider.save(this.remark);
      this.teste = null;
      this.toast.create({message:'Nova observação cadastrada.', duration: 3000}).present();
    }
    else
    {
      this.toast.create({message:'Campo vazio.', duration: 3000}).present();
      return false;
    }

  }

}

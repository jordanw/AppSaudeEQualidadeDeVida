import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditRemarkPage } from './edit-remark';

@NgModule({
  declarations: [
    EditRemarkPage,
  ],
  imports: [
    IonicPageModule.forChild(EditRemarkPage),
  ],
})
export class EditRemarkPageModule {}
